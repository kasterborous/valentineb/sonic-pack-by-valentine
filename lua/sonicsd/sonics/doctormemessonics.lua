SonicSD:AddSonic({
	ID="raniremote",
	Name="Rani's Remote Control",
	ViewModel="models/doctormemes/sonics/raniremote/c_raniremote.mdl",
	WorldModel="models/doctormemes/sonics/raniremote/w_raniremote.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=1,
	SoundLoop = "sonicsd/raniremote/loop.wav",
	DefaultLightColor = Color(137, 0, 0)
})

SonicSD:AddSonic({
	ID="shalkasonic",
	Name="Shalka Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/shalkasonic/c_shalkasonic.mdl",
	WorldModel="models/doctormemes/sonics/shalkasonic/w_shalkasonic.mdl",
	SoundLoop = "sonicsd/shalkasonic/loop.wav",
	LightDisabled = true
})

SonicSD:AddSonic({
	ID="glasses",
	Name="Sonic Sunglasses",
	ViewModel="models/doctormemes/sonics/glasses/c_glasses.mdl",
	WorldModel="models/doctormemes/sonics/glasses/w_glasses.mdl",
	LightDisabled = true
})

SonicSD:AddSonic({
	ID="riversonic",
	Name="River Song's Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/riversonic/c_riversonic.mdl",
	WorldModel="models/doctormemes/sonics/riversonic/w_riversonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=4,
	DefaultLightColor = Color(0, 18, 137)
})

SonicSD:AddSonic({
	ID="cane",
	Name="11th Doctor's Sonic Cane",
	ViewModel="models/doctormemes/sonics/cane/c_cane.mdl",
	WorldModel="models/doctormemes/sonics/cane/w_cane.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=3,
	DefaultLightColor = Color(55, 255, 0)
})

SonicSD:AddSonic({
	ID="lipstick",
	Name="Sarah Jane's Sonic Lipstick",
	ViewModel="models/doctormemes/sonics/lipstick/c_lipstick.mdl",
	WorldModel="models/doctormemes/sonics/lipstick/w_lipstick.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=2,
	DefaultLightColor = Color(255, 0, 89)
})

SonicSD:AddSonic({
	ID="pen",
	Name="Miss Foster's Sonic Pen",
	ViewModel="models/doctormemes/sonics/pen/c_pen.mdl",
	WorldModel="models/doctormemes/sonics/pen/w_pen.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=3,
	DefaultLightColor = Color(4, 0, 255)
})

SonicSD:AddSonic({
	ID="handsonic",
	Name="Hand",
	ViewModel="models/doctormemes/sonics/handsonic/c_handsonic.mdl",
	WorldModel="models/doctormemes/sonics/handsonic/w_handsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=-10,
	SoundLoop = "sonicsd/handsonic/handsonicloop.wav",
	LightDisabled = true
})

SonicSD:AddSonic({
	ID="mcsonic",
	Name="Dark Eyes Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/mcsonic/c_mcsonic.mdl",
	WorldModel="models/doctormemes/sonics/mcsonic/w_mcsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=4,
	DefaultLightColor = Color(25, 82, 255)
})

SonicSD:AddSonic({
	ID="jodiesonic",
	Name="13th Doctor's Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/jodiesonic/c_jodiesonic.mdl",
	WorldModel="models/doctormemes/sonics/jodiesonic/w_jodiesonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=3,
	DefaultLightColor = Color(255, 93, 0)
})

SonicSD:AddSonic({
	ID="transsonic",
	Name="Trans-temporal Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/transsonic/c_transsonic.mdl",
	WorldModel="models/doctormemes/sonics/transsonic/w_transsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=4,
	DefaultLightColor = Color(0, 100, 255)
})

SonicSD:AddSonic({
	ID="trowel",
	Name="River Song's Sonic Trowel",
	ViewModel="models/doctormemes/sonics/trowel/c_trowel.mdl",
	WorldModel="models/doctormemes/sonics/trowel/w_trowel.mdl",
	LightPos=Vector(15,-5,-3),
	LightBrightness=3,
	DefaultLightColor = Color(0, 101, 255)
})

SonicSD:AddSonic({
	ID="smithsonic",
	Name="11th Doctor's Sonic Screwdriver (Extended)",
	ViewModel="models/doctormemes/sonics/smithsonic/c_smithsonic.mdl",
	WorldModel="models/doctormemes/sonics/smithsonic/w_smithsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=3,
	DefaultLightColor = Color(0, 255, 85)
})

SonicSD:AddSonic({
	ID="customsmithsonic",
	Name="11th Doctor's Custom Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/customsmithsonic/c_customsmithsonic.mdl",
	WorldModel="models/doctormemes/sonics/customsmithsonic/w_customsmithsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=4,
	DefaultLightColor = Color(0, 17, 255)
})

SonicSD:AddSonic({
	ID="custombluesonic",
	Name="Custom Blue Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/custombluesonic/c_custombluesonic.mdl",
	WorldModel="models/doctormemes/sonics/custombluesonic/w_custombluesonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=4,
	DefaultLightColor = Color(0, 93, 255)
})

SonicSD:AddSonic({
	ID="customorangesonic",
	Name="Custom Orange Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/customorangesonic/c_customorangesonic.mdl",
	WorldModel="models/doctormemes/sonics/customorangesonic/w_customorangesonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=4,
	DefaultLightColor = Color(71, 255, 43)
})

SonicSD:AddSonic({
	ID="14thdoctorsonic",
	Name="14th Doctor's Sonic Screwdriver",
	ViewModel="models/doctormemes/sonics/14thdoctorsonic/c_14thdoctorsonic.mdl",
	WorldModel="models/doctormemes/sonics/14thdoctorsonic/w_14thdoctorsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=4,
	DefaultLightColor = Color(0, 72, 255)
})